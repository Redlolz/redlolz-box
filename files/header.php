<link rel="stylesheet" href="/style/main.css">
<link rel="stylesheet" href="/style/files.css">
<div id="header">
	<a href="/" id="title">Redlolz Box</a>
	<ul id="menu">
		<li><a href="/">Home</a></li>
		<li><a href="/files">Files</a></li>
		<li><a href="/chat/">Chat</a></li>
		<li><a href="/about/">About</a></li>
	</ul>
</div>

<div id="upload">
	<form action="/files/upload.php" method="post" enctype="multipart/form-data">
		Select file to upload [Max size of 50MB]:
		<input type="file" name="fileToUpload" id="fileToUpload">
		<input type="submit" value="Upload file" id="submit" name ="submit">
	</form>
</div>
