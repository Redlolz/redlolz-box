<?php
$target_dir = 'user_submitted/';
$target_file = $target_dir . basename($_FILES['fileToUpload']['name']);
$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$uploadOk = 1;

if (file_exists($target_file)) {
	echo "File already exists<br>";
	$uploadOk = 0;
}

$bannedFiles = array('html', 'php', 'js', 'htm', 'xhtml', 'htaccess');

if (in_array($fileType, $bannedFiles)) {
	echo "We can not let you upload that filetype for security reasons<br>";
	$uploadOk = 0;
}

if ($_FILES['fileToUpload']['size'] > 50000000) {
	echo "File too large<br>";
	$uploadOk = 0;
}

if ($uploadOk == 0) {
	echo "Your file was not uploaded";
}
else {
	if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_file)) {
		echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded";
	}
	else {
		echo "There was an unknown error while uploading your file";
	}
}
echo "<br><a href='/files'>Back to Files</a>";
?>
