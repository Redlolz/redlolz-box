#!/bin/bash

echo "Redlolz Box Deployer"

if [ "$EUID" -ne 0 ]
	then echo "Please run as root or use sudo"
	exit
fi

echo "Updating apt..."
apt update

echo "Installing required packages..."
apt install apache2 dnsmasq git hostapd php -y

echo "Configuring files..."

nmcli device status
echo "Specify your network card (Most likely wlan0 but could be different):"
read -p "Interface: " interface

echo "Configuring dhcpcd..."
dhcpcd_file="/etc/dhcpcd.conf"
if grep -q "# Redlolz Box static ip" $dhcpcd_file;
then
	echo "dhcpcd already configured"
else
	echo "# Redlolz Box static ip
	interface $interface
	    static ip_address=192.168.4.1/24
	    nohook wpa_supplicant" >> $dhcpcd_file
fi

echo "Configuring dnsmasq..."
mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
echo "# Redlolz Box DHCP
interface=$interface
dhcp-range=$interface ,192.168.4.1,192.168.4.20,255.255.255.0,24h
address=/#/192.168.4.1" > /etc/dnsmasq.conf

echo "Configuring hostapd..."
echo "interface=$interface
driver=nl80211
ssid=Redlolz Box
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0" > /etc/hostapd/hostapd.conf
echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' >> /etc/default/hostapd

echo "Routing and stuff..."
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables-save > /etc/iptables.ipv4.nat
echo '#!/bin/sh -e
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
	printf "My IP address is %s\n" "$_IP"
fi
iptables-restore < /etc/iptables.ipv4.nat
exit 0' > /etc/rc.local

rm -rf /var/www/html/*
git clone https://gitlab.com/redlolz/redlolz-box /var/www/html
chmod 755 /var/www/html
chown -R www-data:www-data /var/www/html

echo "Enabling services..."
systemctl unmask hostapd
systemctl enable hostapd
systemctl enable dnsmasq

echo "Done. Rebooting... (this will close the ssh connection)"
reboot
