<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Chat</title>
    
    <link rel="stylesheet" href="/style/chat.css" type="text/css">
    <link rel="stylesheet" href="/style/main.css" type="text/css">
    
    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="chat.js"></script>
    <script type="text/javascript">
// kick off chat
var chat =  new Chat();
$(function() {

	chat.getState(); 

	// watch textarea for key presses
	$("#sendie").keydown(function(event) {  

		var key = event.which;  

		//all keys including return.  
		if (key >= 33) {

			var maxLength = $(this).attr("maxlength");  
			var length = this.value.length;  
		}  
	});
	// watch textarea for release of key press
	$('#sendie').keyup(function(e) {	

		if (e.keyCode == 13) { 

			var text = $(this).val();
			
			var name = document.getElementById('sender-name').value;
			name = name.replace(/(<([^>]+)>)/ig,"");

			chat.send(text, name);	
			$(this).val("");
		}
	});

});
    </script>

</head>

<body onload="setInterval('chat.update()', 1000)">
	<header>
<?php
include $_SERVER['DOCUMENT_ROOT'].'/header.php';
?>
	</header>

    <div id="page-wrap">
    
        <h2>Chat</h2>
        
		<p id="name-area">
		</p>
        
		<div id="chat-wrap">
			<div id="chat-area">
<?php
echo file_get_contents('chat.txt');
?>
			</div>
		</div>
        
        <form id="send-message-area">
			<p>Your username:</p>
			<textarea id="sender-name">Guest</textarea>
			<p>Your message: </p>
            <textarea id="sendie" autofocus></textarea>
        </form>
    
    </div>

</body>

</html>
